﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossScript : MonoBehaviour
{
    public int health;
    public GameObject explosion;
    public GameObject playerExplosion;
    public int score;
    private GameController gameController;


    // Start is called before the first frame update
    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0f)
        {

            Destroy(gameObject);
            gameController.Winner();
            gameController.restartText.text = "Press 'R' to Restart!";
            gameController.restart = true;
            Instantiate(explosion, transform.position, transform.rotation);
            gameController.AddScore(score);
            StartCoroutine(gameController.SpawnWaves());
        }
        
    }

    void OnTriggerEnter (Collider other)
    {
        health -= 1;
        Instantiate(explosion, transform.position, transform.rotation);

        if (other.tag == "Player")
        {
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            gameController.GameOver();
            gameController.restartText.text = "Press 'R' to Restart!";
            gameController.restart = true;

        }
        Destroy(other.gameObject);

        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin;
    public float xMax;
    //public float yMax;
    //public float yMin;
    public float zMin;
    public float zMax;
}

public class PlayerController : MonoBehaviour
{
    private Rigidbody rb;
    private GameController gameController;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
    }
    public float speed;
    public float tilt;
    public Boundary boundary;

    public int bombAmount;
    public GameObject bomb;
    public Transform[] bombSpawns;
    public GameObject shot;
    public Transform[] shotSpawns;
    public float fireRate;
    public bool TriFireOn = false;

    private float nextFire;
    private float bombFire;

    void Update()
    {
        if (gameController.score >= 700)
        {
            TriFireOn = true;
        }
        if (gameController.score >= 1000)
        {
            fireRate = 0.1f;
        }
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            if (TriFireOn)
            {
                for (int i = 0; i < shotSpawns.Length; i++)//iterate thorugh the shot spawn array
                {
                    Instantiate(shot, shotSpawns[i].position, shotSpawns[i].rotation);//Spot shots at position
                }
                GetComponent<AudioSource>().Play();
            }
            else
            {
                Instantiate(shot, shotSpawns[0].position, shotSpawns[0].rotation);
                GetComponent<AudioSource>().Play();
            }
        }
        if (Input.GetButton("Fire2") && bombAmount > 0 && Time.time > bombFire)
        {
            bombFire = Time.time + fireRate;
            for (int i = 0; i < bombSpawns.Length; i++)
            {
                Instantiate(bomb, bombSpawns[i].position, bombSpawns[i].rotation);
                bombAmount -= 1;
                gameController.BombTextUpdate();
            }
            
        }
    }

    void FixedUpdate ()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.velocity = movement * speed;

        rb.position = new Vector3 
            (
            Mathf.Clamp (rb.position.x, boundary.xMin, boundary.xMax),
            0.0f,
            Mathf.Clamp (rb.position.z, boundary.zMin, boundary.zMax)
            );

        rb.rotation = Quaternion.Euler(0.0f, 0.0f, rb.velocity.x * -tilt);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Pickup")
        {
            gameController.OneUp();
            Destroy(other.gameObject);
        }
        if (other.tag == "BombUp")
        {
            bombAmount += 1;
            gameController.BombUp();
            Destroy(other.gameObject);
        }
    }
}

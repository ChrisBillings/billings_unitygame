﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public GameObject Boss;
    public Vector3 BossSpawn;
    public Quaternion BossSpawnRot;
    public int Lives;
    public int bombs;
    public GameObject player;
    public Vector3 playerSpawn;
    public GameObject[] hazards;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    public Text bombText;
    public Text lifeText;
    public Text scoreText;
    public Text restartText;
    public Text gameOverText;
    public int score;

    private bool gameOver;
    public bool restart;
    void Start()
    {
        bombText.text = "Bombs: 2";
        lifeText.text = "Lives: 3";
        gameOver = false;
        restart = false;
        restartText.text = "";
        gameOverText.text = "";
        score = 0;
        UpdateScore();
        StartCoroutine (SpawnWaves());
    }
    void Update()
    {
        if (restart)
        {
            if (Input.GetKeyDown (KeyCode.R))
            {
                Application.LoadLevel(Application.loadedLevel);
            }
        }
    }
    


    public IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        for (int j = 0; j < 10; j++)//makes 10 waves
        {
            for (int i = 0; i < hazardCount; i++)
            {
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);
           
            if (gameOver)
            {
                restartText.text = "Press 'R' to restart";
                restart = true;
                break;
            }

        }
        Instantiate(Boss, BossSpawn, BossSpawnRot);//releases the boss after all waves
    }  
    
    public void AddScore (int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }
    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }
    public void GameOver ()
    {
        gameOverText.text = "Game Over!";
        gameOver = true;
    }
    public void Winner ()
    {
        gameOverText.text = "You Win!";
        gameOver = true;
    }
    public void Respawn ()//called when player get destroyed. 
    {
        if (Lives == 0)//checks life counter
        {
            GameOver();
        }
        player.transform.position = playerSpawn;//if not 0 updates the counter and text
        player.SetActive(true);
        Lives = Lives - 1;
        lifeText.text = "Lives: " + Lives;
    }
    public void OneUp ()//makes pickup 1 add an extra life
    {
        Lives += 1;
        lifeText.text = "Lives: " + Lives;
    }
    public void BombUp()//adds a bomb
    {
        bombs += 1;
        bombText.text = "Bombs: " + bombs;
    }
    public void BombTextUpdate()//yeets that bomb outta here
    {
        bombs -= 1;
        bombText.text = "Bombs: " + bombs;
        
    }
}

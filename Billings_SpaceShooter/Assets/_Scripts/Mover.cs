﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Mover : MonoBehaviour
{

    private Rigidbody rb;

    public float speed;
    //public Boundary boundary;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        rb.velocity = transform.forward * speed;
    }

   // private void FixedUpdate()
    //{
       // rb.position = new Vector3
        //    (0.0f, Mathf.Clamp
   // }
}

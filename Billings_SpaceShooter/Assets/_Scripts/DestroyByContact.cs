﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour
{
    public GameObject explosion;
    public GameObject playerExplosion;
    public int scoreValue;
    private GameController gameController;
    private PlayerController playerController;

    public GameObject[] pickupPrefabs;
    GameObject Pickup;

    

    
  

    void Start ()
    {
        
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        GameObject playerControllerObject = GameObject.FindWithTag("Player");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
        if (playerControllerObject != null)
        {
            playerController = playerControllerObject.GetComponent<PlayerController>();
        }
        
    }

    void OnTriggerEnter(Collider other)
    {
        //if (other.tag == "Boundary")
        if (other.CompareTag("Boundary") || other.CompareTag("Enemy")) 
        {
            //SpawnPickup();
            return;
        }
        //Debug.Log("Is there an enemy here");
        if (gameObject.tag == "Enemy")
        {
            if (other.tag != "player")
            {
                float Chances = Random.value;
                if (Chances > .95)//spawn chances of pickups 
                {
                    SpawnPickup(other);
                }
                //if (Chances < .1)
                //{ }
            }
            //Debug.Log("I wont spawn for some reason");
            
            //if (Pickup == null)
            //{

            //}

        }
        
        if (explosion != null)
        {
            Instantiate(explosion, transform.position, transform.rotation);
        }
        if (other.tag == "Player") { 
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            
            other.gameObject.SetActive(false);//sets player inactive ready for respawn
            gameController.Respawn();//calls respawn
        }
        gameController.AddScore(scoreValue);
        if (tag != "Bomb")
        {
            Destroy(gameObject);
        }

        
    }

    void SpawnPickup(Collider other)
    {
        GameObject pickupToSpawn = pickupPrefabs[(int)(Random.value * pickupPrefabs.Length)];
        Pickup = Instantiate(pickupToSpawn, other.transform.position, pickupToSpawn.transform.rotation);
        //makes pickups 
    }
}
